Install dependencies: `npm install`

Run dev server: `npm run start`

Build production version: `npm run build`

Start server: `node app.js`
