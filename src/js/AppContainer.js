import React from "react";
import store from "./positions";
import FormContainer from "./FormContainer";
import JobListContainer from "./JobListContainer";
import {Provider} from "react-redux";

const AppContainer = () => (
    <Provider store={store}>
        <div style={{margin: 'auto', maxWidth: 900}}>
            <FormContainer/>
            <JobListContainer positions={store.getState()}/>
        </div>
    </Provider>
);

export default AppContainer;
