import React from "react";
import Grid from "@material-ui/core/Grid";

const blue = "#2196f3";
const gray = "#BDBDBD";
const darkGray = "#757575";
const green = "#52b202";

const PositionContainer = ({position}) => (
    <div>
        <Grid container style={{marginBottom: 8}}>
            <Grid item xs={7} style={{fontWeight: "bold", color: blue}}>
                {position.name}
            </Grid>
            <Grid item xs={5} style={{color: darkGray, textAlign: "right"}}>
                {position.location}
            </Grid>
        </Grid>
        <Grid container spacing={0}>
            <Grid item xs={7} style={{color: gray}}>
                {position.company} - <span style={{color: green}}>{position.jobType}</span>
            </Grid>
            <Grid item xs={5} style={{fontWeight: "bold", color: gray, textAlign: "right"}}>
                {position.created}
            </Grid>
        </Grid>
    </div>
);

export default PositionContainer;