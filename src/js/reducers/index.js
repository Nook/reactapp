import {GET_POSITIONS} from "../constants";

const initialState = {
    positions: [
        {id: 1, name: 'dd', location: 'ff', company: 'gg', jobType: 'hh', created: 'jj'},
        {id: 2, name: 'zz', location: 'xx', company: 'cc', jobType: 'vv', created: 'bb'},
        {id: 3, name: 'qq', location: 'ww', company: 'ee', jobType: 'rr', created: 'tt'},
    ]
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POSITIONS:
            state.positions = action.payload;
            return state;
        default:
            return state;
    }
};

export default rootReducer;
