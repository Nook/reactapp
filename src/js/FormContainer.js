import React from "react";
import Button from '@material-ui/core/Button';
import Checkbox from "@material-ui/core/es/Checkbox/Checkbox";
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Paper from "@material-ui/core/es/Paper/Paper";
import {getPositions} from "./actions";
import {connect} from "react-redux";

const GET_POSITIONS = 'https://jobs.github.com/positions.json';

const mapDispatchToProps = dispatch => {
    return {
        getPositions: positions => dispatch(getPositions(positions))
    };
};

class FormContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fullTime: false,
            description: "",
            location: "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        // this.loadData();
    }

    handleSubmit(event) {
        event.preventDefault();
        // this.loadData();
    }

    handleClick(event) {
        this.setState({[event.target.id]: !this.state.fullTime});
    }

    handleChange(event) {
        this.setState({[event.target.id]: event.target.value});
    }

    loadData() {
        this.props.getPositions([]);

        let url = new URL(GET_POSITIONS);
        Object.keys(this.state).forEach(key => url.searchParams.append(key, this.state[key]));

        fetch(url.href)
            .then(res => res.json())
            .then(
                (result) => {
                    let positions = [];
                    for (let item of result) {
                        positions.push({
                            id: item['id'],
                            name: item['title'],
                            company: item['company'],
                            jobType: item['type'],
                            location: item['location'],
                            created: item['created_at']
                        });
                    }

                    this.props.getPositions(positions);
                },
                (error) => {}
            );
    }

    render() {
        return (
            <Paper style={{padding: 16}}>
                <form id="article-form" onSubmit={this.handleSubmit}>
                    <Grid container spacing={8}>
                        <Grid item sm={3} xs={12}>
                            <TextField
                                style={{width: "100%"}}
                                id="description"
                                label="Job Description"
                                value={this.state.description}
                                onChange={this.handleChange}
                            />
                        </Grid>
                        <Grid item sm={3} xs={12}>
                            <TextField
                                style={{width: "100%"}}
                                id="location"
                                label="Location"
                                value={this.state.location}
                                onChange={this.handleChange}
                            />
                        </Grid>
                        <Grid item sm={3} xs={12}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.state.fullTime}
                                        onChange={this.handleClick}
                                        id="fullTime"
                                        color="primary"
                                    />
                                }
                                label="Full Time Only"
                            />
                        </Grid>
                        <Grid item sm={3} xs={12}>
                            <Button size="large"
                                    type="submit"
                                    variant="contained"
                                    color="primary">
                                Search
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
        );
    }
}

const Form = connect(null, mapDispatchToProps)(FormContainer);
export default Form;
