import {GET_POSITIONS} from "../constants";

export const getPositions = positions => ({type: GET_POSITIONS, payload: positions});