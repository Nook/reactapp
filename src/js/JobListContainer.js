import React from "react";
import Typography from "@material-ui/core/es/Typography/Typography";
import Table from "@material-ui/core/es/Table/Table";
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";
import TableBody from "@material-ui/core/es/TableBody/TableBody";
import Paper from "@material-ui/core/es/Paper/Paper";
import {connect} from "react-redux";
import PositionContainer from "./PositionContainer";

const mapStateToProps = state => {
    return {positions: state.positions};
};

const JobList = ({positions}) => (
    <Paper style={{marginTop: 16, padding: 16}}>
        <Typography variant="h4" gutterBottom>
            Showing {positions.length} jobs
        </Typography>
        <Table>
            <TableBody>
                {positions.map(row => (
                    <TableRow key={row.id}>
                        <TableCell>
                            <PositionContainer position={row}/>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    </Paper>
);

const List = connect(mapStateToProps)(JobList);
export default List;
