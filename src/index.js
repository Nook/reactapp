import ReactDOM from "react-dom";
import React from "react";
import AppContainer from "./js/AppContainer";

const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(<AppContainer />, wrapper) : false;

